(function ($) {
    "use strict";
    var GAME = {};
    GAME.pageN = 0;
    GAME.point = 0;
    GAME.ele = {
        btn: $(".btn"),
        page: $(".page"),
        result: $(".result"),
        cover: $(".cover"),
        input: $(".input")
    };
    GAME.checkAnswer = function () {
        var ans = ["蜘蛛", "六神无主", "尔", "雨伞", "闷", "自食其果", "明", "蚊香", "无独有偶", "胖"];
        GAME.ele.input.each(function (index, element) {
            if ($(element).val() === ans[index]) {
                GAME.point += 1;
            }
        });
        GAME.ele.result.text("共答对：" + GAME.point + " 题");
        $.fn.wxshare({
            title: "猜灯谜 闹元宵 红包大奖等你拿",
            imgUrl: "http://comic.qianlong.com/wx/img/wxshare.jpg",
            desc: "正月十五闹元宵 猜灯谜来抢红包 上元灯节烟花好 行歌伴得夜光照",
            audio: "",
            bdname: "zhengyueshiwu",
            bdfunc: "share",
            bdvalue: "weixin",
            shareSuccessCall: function () {
                GAME.ele.page.removeClass("active");
                GAME.ele.page.eq(12).addClass("active");
            }
        });
    };
    GAME.jumpPage = function () {
        var way = $(this);
        if (way.hasClass("btn-start") || way.hasClass("btn-next") || way.hasClass("btn-submit")) {
            GAME.pageN += 1;
        } else if (way.hasClass("btn-pre")) {
            GAME.pageN -= 1;
        }
        GAME.ele.page.removeClass("active");
        GAME.ele.page.eq(GAME.pageN).addClass("active");
        if (way.hasClass("btn-share")) {
            GAME.ele.cover.show();
        }
        if (way.hasClass("btn-submit")) {
            GAME.checkAnswer();
        }
    };
    GAME.checkPlatform = function () {
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) === "micromessenger") {
            return true;
        } else {
            return false;
        }
    };
    if (!GAME.checkPlatform()) {
        window.alert("获得最佳体验，请用微信扫一扫~");
    }
    GAME.ele.btn.click(GAME.jumpPage);
}(window.jQuery));